#include <iostream>
#include <string>

#define SIZE (20)

using namespace std;

//Battleのクラス
class Battle{
    int Time;
    char Present_gold[SIZE];
public:
    void Present_fallen();
    void Battle_anata();
    void Battle_Rival();
};

//class_Battleの派生_anata
class you_anata : public Battle{
    int HP;
    int power;
    int walk_count;
public:
    int WalkCount(int n);
    int Attack(int m);
    int Defence(int o);
};
//プレイヤーのバトル中のアタック
int you_anata::Attack(int m){
    power = WalkCount(walk_count);
    return (power);
}
//プレイヤーのバトル中のディフェンス
int you_anata::Defence(int o){
    
    return(HP);
}

//class_Battleの派生_Rival
class Rival_Akuma : public Battle{
    int HP;
    int power;
public:
    void Attack();
    void Defence();
};
//敵のバトル中のアタック
void Rival_Akuma::Attack(){
    
}
//敵のバトル中のディフェンス
void Rival_Akuma::Defence(){
    
}


//Mo_keの進化におけるclass
class Mo_Ke_Rathout{
    char LightnigStone;
    char Rath;
    char Rathout;
    char Rathoutal;
    char Rathoutalis;
public:
    void Involved(char Rath, char Rathout, char Rathoutal, char Rathotalis);
    void Stone(char LightningStone,char Rathout,char Rathoutal);
    void Natsukido(char Rath, char Rathout, char Rathoutalis) ;
};
//イベントが発生するごとにカウントを進める（タマゴが孵化するまで実行[予定]）
int you_anata::WalkCount(int walk_count){
    
    return 0;
}

void Mo_Ke_Rathout::Involved(char Rath, char Rathout, char Rathoutal, char Rathotalis)
{
    
    
}
void Mo_Ke_Rathout::Stone(char LightningStone,char Rathout,char Rathoutal)
{
    
    
}
void Mo_Ke_Rathout::Natsukido(char Rath, char Rathout,char Rathoutalis)
{
    
    
}

int sexual_decision(int sexual);
void event_prologue();
void event_prologue2(string name1,int sexual);
void event_forest_area(string name1,int sexual);
void event_tera_area(string name1,int sexual);
void event_forest_area_1(string name1,int sexual);
void event_forest_area_2(string name1,int sexual);
void Encount_Friend1(string name1,int sexual);

int main()
{
    int gender,sexual;
    int choosing1;
    int walk_count;//予定
    char name[SIZE];
    
    
    event_prologue();
    getchar();
    
    gender = sexual_decision(sexual);
    getchar();
    
    while(1){
        if(gender==1){
            cout << "君の名前は何というのかね？\n入力：";
            cin >> name;
            cout << name << "くんというのか、よい名前だ。\n \n";
            break;
        }
        else if(gender==2){
            cout << "君の名前は何というのかね？\n入力：";
            cin >> name;
            cout << name <<"ちゃんというのか、よい名前だ。\n \n";
            break;
        }
        else{
            continue;
        }
    }

    getchar();
    
    cout << "私の名前はテトラ。\n今から"<< name <<", 君は壮大な旅のプロローグを辿ろうとしている。\n";
    getchar();
    
    cout <<"君はこの旅の中でいろんなことを経験するであろう。\n";
    getchar();
    
    cout << "様々な出会い、そして別れ。\n";
    getchar();
    
    cout << "その一つ一つを直視し経験していくであろう。\n";
    getchar();
    
    cout  << "ほら、もう世が明けたようじゃよ、"<< name <<"、君の新たな物語の始まりじゃ。\n";
    getchar();
    
    cout << "気がつくと、空にはもう陽が照っていた。\n見渡すとそこにあるのはいつもの荒んだ光景だ。\n昨日のあれは夢だったのか・・・\n";
    getchar();
    
    cout << "そう思った束の間、何度かもつれて修繕したリュックがいつもより重いことに気づいた。\n開くと、そこには昨日渡された'もけ'のタマゴと一枚の紙切れが入っていた\n";
    getchar();
    
    cout << "紙切れにはこう書かれていた\n";
    getchar();
    
    cout << name <<"、　お前さんにそのタマゴと一緒に行動してほしいのじゃ。\nそれを持っていることで危険が伴うことは"<< name <<"もわかっておるじゃろう。\n";
    getchar();
    
    cout << "しかし、君が選ばれし者である以上、いつかは通る道なのだよ。\nこの言葉もいづれわかる日が来る。\n今はそのタマゴをもって行動してほしい。\n";
    getchar();
    
    cout << "健闘を祈る・・・・\n";
    getchar();
    
    cout << "とりあえず、辺りを見渡してみた。";
    getchar();
    
    cout << "現在の手持ちは、少量の食料と工具の類いと銅貨が三枚ほどである\nそして、目の前は荒れ果てた荒野が広がっている。";
    getchar();
    
    cout << "右手には森林が広がっているようだ。\n \n";
    while(1){
        cout << "どうしますか？\n①：森林のある方へ進む　②：荒野のある方へ進む\n入力：";
        cin >> choosing1;
        if(choosing1==1){
            event_forest_area(name,gender);
            break;
        }
        else if(choosing1==2){
            event_tera_area(name,gender);
            break;
        }
        else{
            continue;
        }
    }
    
    exit (0);
}
//序章
void event_prologue()
{
    cout << "この物語はある少年が'anata'となり、平和が訪れた400年後の話・・・・\n";
    getchar();
    cout << "全国各地にMo:Ke（もけ）を討伐できるようになったきっかけである技術革命が起こり、\nほぼすべてのMo:Keが斬殺されるという大虐殺事件そして、\n反自然派と自然派との大戦争が起こった後の話である。"<< "\n";
    getchar();
    cout << "？？？\n「ワシはナダギじゃ。\n" <<"  君にとある'もけ'が持っていたタマゴを持っていってほしいのじゃ！！」\n";
    getchar();
    cout << "ナダギ\n「おっと・・・誰かが来たようじゃ。\n"<<"  ワシはこの町を去らねばならないのじゃよ・・・・・。\n" << "  ではさらばじゃ！！！」";
    getchar();
    cout << "サッサっッツ・・・・・・・・。\n \n";
    getchar();
    cout << "'もけ'のタマゴとともにその老人は一片の紙切れを渡した。\n急なことで何が起こったのかわからないままあなたは、もうとっくに暗くなっていた空を眺め眠りについた。\n";
    getchar();
    cout << "君はこの物語の主人公となる運命なのかもしれない。\n"<<"君の選択次第でその運命は決まってしまうだろう。\n";
    getchar();
    cout << "それでは・・・・・・・・\n";
    getchar();
    cout << "\n\n・・・・・・・・\n\n";

}
//性別決定(文字入力されるとエラー出)
int sexual_decision(int sexual)
{
    int a;
    
    while(1){
        cout << "君は男の子かな？女の子かな？\n"<<"①：男の子 ②：女の子\n";
        cout << "入力："; cin >> sexual;
        if(sexual==1){
            while(1){
                cout << "君は男の子であってるかな？\n"<<"①：はい or ②：いいえ\n";
                cout << "入力："; cin >> a;
                if(a==1){
                    break;
                }
                else if(a==2){
                    break;
                }
                else{
                    continue;
                }
            }
            if(a==1){
                break;
            }
            if(a==2){
                continue;
            }
            else{
                continue;
            }
        }
        else if(sexual==2){
            while(1){
                cout << "君は女の子であってるかな？\n"<<"①：はい or ②：いいえ\n";
                printf("入力："); cin >> a;
                if(a==1){
                    break;
                }
                else if(a==2){
                    break;
                }
                else{
                    continue;
                }
            }
            if(a==1){
                break;
            }
            if(a==2){
                continue;
            }
            else{
                continue;
            }
        }
        else{
            continue;
        }
        break;
    }
    return (sexual);
}
//荒廃エリアのストーリー
void event_tera_area(string name1,int sexual)
{
    int sord;
    cout << "荒廃した、町が見えてきた。\nしばらく歩いていると、町の片隅に飢えた人であろう死骸が無惨にころがっていた。\n";
    getchar();
    cout << "死骸の手には何かが握られている。\n数十センチある短剣のようだ。\n";
    getchar();
    cout << "①：短剣は持っていた方が身のためだ。\n　　もう生きていないのだからもらっておこう。\n";
    cout << "②：他人の物をとってはダメだ\n　　近くに野花がある、お供えしてあげよう・・・\n";
    while(1){
        cout << "入力："; cin >> sord;
        switch(sord){
            case 1: cout << "剣を抜いて身構えていたが至って何も起こらなかった。\nそのまま、先へと足を進めた。\n"; break;
            case 2: cout << "花を供えそのままその場を立ち去った。\n町の外には盗賊がいるようだ。\n";
                    cout << "その場しのぎをしようと考え、死骸の隙間に隠れた。\n";break;
            default: cout << "もう一度入力してください\n"; continue;
        }
        break;
    }
}
//森林エリアのストーリー
void event_forest_area(string name1,int sexual)
{
    int Friend1;
    
    cout << "心地よい風が吹いていて、木漏れ陽が暖かい。\n";
    getchar();
    cout << "木々の隙間から空が見える場所に出た。\n切り株に腰をおろし、いつの間にかうとうとしてくる。\n";
    getchar();
    cout << "そよ風とともにゆっくりとした時間が流れる、そんな一瞬がずっと続けばいいなと願っていた。\n";
    getchar();
    cout << "・・・・・・\n";
    getchar();
    cout << "気がつくと、辺りはもうすっかり暮れなずんでいる。\n";
    getchar();
    cout << "夜になると危険だろう・・・。とにかくこの森林を抜けたほうがよさそうだ。\n";
    getchar();
    cout << "そう思った束の間・・・";
    getchar();
    cout << "どこからか、足音が聞こえてきた。\n";
    getchar();
    cout << "①：身を潜めて様子を見よう。 ②：盗賊に出会うと大変だ。すぐに逃げよう。\n";
    cout << "入力："; cin >> Friend1;
    while(1){
        switch(Friend1){
            case 1 : Encount_Friend1(name1,sexual); break;//森林エリアのイベント-新しい友達
            case 2 : cout << "足音をひそめ、できる限り、急いで森林をぬけた。"; break;
            default : cout << "もう一度入力してください\n"; continue;
        }
        break;;
    }
}
//森林エリアのイベント-新しい友達
void Encount_Friend1(string name1,int sexual)
{
    cout << "ガサｯガサガサﾂ・・・\n";
    getchar();
    
    cout << "ふいに音がした方向に目をやると、誰かに姿を見られてしまったようだ。\n";
    getchar();
    
    cout << "？？？\n「誰..？そこにいるのは...\n";
    
}

